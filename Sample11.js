var http = require("http");
var fs = require("fs");


function send404response(response)
{
    response.writeHead(404, {"Content-Type": "text/plain"});
    response.write("Error 404, page not found!");
    response.end();
}

function onRequest(request, response)
{
    if (request.method == 'GET' && request.url == '/')
        {
        response.writeHead(200, {"Content-Type": "text/html"});
        response.write("ggg");
        fs.createReadStream("./index.html").pipe(response);
        }
        else
        {
        send404response(response);
        }
}

http.createServer(onRequest).listen(process.env.PORT, process.env.IP);
console.log("Server is now running");