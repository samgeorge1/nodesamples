var path = require("path");

var websitehome = 'desktop/nodesamples/readme.text';
var websiteabout = 'desktop/nodesamples/tesco.txt';

console.log(path.normalize(websitehome));
console.log(path.dirname(websitehome));
console.log(path.basename(websitehome));
console.log(path.extname(websitehome));

// below command calls in periodic intervals
setInterval(function(){console.log("Hello")}, 2000 );